<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $auth_key
 * @property integer $created
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     *
     */
   // public $password_h;

    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           // [['username', 'email', 'password'], 'filter', 'trim'],
            [['username', 'email', 'password'], 'filter', 'filter' => 'trim'],
            [['username', 'email'], 'required'],
            ['email', 'email'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['password', 'required', 'on' => 'create'],
            ['username', 'unique', 'message' => 'Это имя занято.'],
            ['email', 'unique', 'message' => 'Эта почта уже зарегистрирована.'],
            ['secret_key', 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
            'created' => 'Created',
        ];
    }
    public function behaviors() {
        return [
            [
                'class'=>TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'update'
            ]
        ];
    }


    public static function findByUsername($username) {
        return static::findOne([
            'username'=>$username
        ]);
    }

    public static function findIdentity($id) {
        return static::findOne(['id'=>$id]);
    }
    public function getAuthKey() {
        $this->auth_key;
    }

    public function validateAuthKey($key) {
        return $this->auth_key === $key;
    }

    /* ������ */

    public function setPassword($password) {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey () {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function validatePassword ($password) {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function getId() {
        return $this->id;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

//    public function generateSecretKey()
//    {
//        $this->secret_key = Yii::$app->security->generateRandomString().'_'.time();
//    }
//    public static function isSecretKeyExpire($key)
//    {
//        if (empty($key))
//        {
//            return false;
//        }
//        $expire = Yii::$app->params['secretKeyExpire'];
//        $parts = explode('_', $key);
//        $timestamp = (int) end($parts);
//        return $timestamp + $expire >= time();
//    }
//
//    public function removeSecretKey()
//    {
//        $this->secret_key = null;
//    }
//
//    public static function findBySecretKey($key)
//    {
//        if (!static::isSecretKeyExpire($key))
//        {
//            return null;
//        }
//        return static::findOne([
//            'secret_key' => $key,
//        ]);
//    }

    public function generateAccessToken()
    {
        $this->access_token = Yii::$app->security->generateRandomString();
    }

    public function updateAccessToken()
    {
        $this->access_token = Yii::$app->security->generateRandomString();
        $this->save(false);
    }
}
