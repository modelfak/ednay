<?php

namespace app\models;
use \yii\behaviors\TimestampBehavior;

use Yii;

/**
 * This is the model class for table "books".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date_create
 * @property integer $date_update
 * @property string $preview
 * @property integer $date
 * @property integer $author_id
 *
 * @property Authors $author
 */
class Books extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const pathFile = 'uploads/';
    const pathFileThumb = 'uploads/thumb/';
    const widthThumb = 70;
    const heightThumb = 70;

    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['name', 'date_create', 'date_publish', 'author_id'], 'required', 'on'=>'default'],

                ['date_create', 'date', 'format' => 'yyyy-MM-dd'],
                ['date_publish', 'date', 'format' => 'yyyy-MM-dd'],
                [['name', 'preview'], 'string'],
                [['author_id'], 'integer'],
                [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Authors::className(), 'targetAttribute' => ['author_id' => 'id']],
                //[['date_create', 'date_publish'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
//    public function behaviors() {
//        return [
//            [
//                'class'=>TimestampBehavior::className(),
//                'createdAtAttribute' => 'date_create',
//                'updatedAtAttribute' => 'date_update'
//            ]
//        ];
//    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            //var_dump($this->date_create);exit;
               if(!empty($this->date_create)) {
                   $this->date_create = strtotime($this->date_create);
               }
                if(!empty($this->date_publish)) {
                    $this->date_publish = strtotime($this->date_publish);
                }
            return true;
        }
        return false;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            // delete file
            if(!empty($this->preview)) {
                if(file_exists(self::pathFile . $this->preview)) {
                    @unlink(self::pathFile . $this->preview);
                }
                if(file_exists(self::pathFileThumb . $this->preview)) {
                    @unlink(self::pathFileThumb . $this->preview);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function beforeValidate () {
        if (parent::beforeValidate()) {
            if(!empty($this->date_publish) && is_numeric($this->date_publish)) {
                $this->date_publish = date('Y-m-d', $this->date_publish);
            }
            if(!empty($this->date_create) && is_numeric($this->date_create)) {
                $this->date_create = date('Y-m-d', $this->date_create);
            }

            return true;
        }
        return false;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'date_create' => 'Дата создания',
            'date_publish' => 'Дата публикации',
            'preview' => 'Превью',
            'author_id' => 'Автор',
            'author' => 'Автор'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Authors::className(), ['id' => 'author_id']);
    }

    public function getImageurlThumb()
    {
        $path = self::pathFileThumb . $this->preview;
       if(file_exists($path) && $this->preview) {
           return DIRECTORY_SEPARATOR.$path;
       }else{
           return DIRECTORY_SEPARATOR.'images/noimage.jpg';
       }

    }

    public function getImageurl()
    {
        $path = self::pathFile .$this->preview;
        if(file_exists($path) && $this->preview) {
            return DIRECTORY_SEPARATOR.$path;
        }else{
            return DIRECTORY_SEPARATOR.'images/noimage.jpg';
        }

    }

}
