<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.MySQL - test@localhost
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $lg = Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
            $user->updateAccessToken();
            return $lg;
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

//    public function requestForAccessToken()
//    {
//        /*
//         * with username and password to login apiHost for user/id's access_token
//         * after the basic authentication
//         * token will be generated at the api server end
//         */
//        $username = $this->username;
//        $password = $this->password;
//        //$id = $this->_user->id;
//        $apiHost = Yii::$app->params['restapi']['apiHost'];
//
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $apiHost.'/books');
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
//        curl_setopt($ch, CURLOPT_HEADER, FALSE);
//       // curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//        curl_setopt($ch, CURLOPT_USERPWD, $username.':'.$password);
//        $response = curl_exec($ch);
//        $ff = 'ddfg';
//        curl_close($ch);
//        var_dump($response);exit;
//    }
}
