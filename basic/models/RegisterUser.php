<?php
namespace app\models;
use yii\base\Model;
use Yii;
class RegisterUser extends Model
{
    public $username;
    public $email;
    public $password;

    public function rules()
    {
        return [
//            [['username', 'email', 'password'],'filter', 'filter' => function($value) {
//                return trim(htmlentities(strip_tags($value), ENT_QUOTES, 'UTF-8'));
//            }],
            [['username', 'email', 'password'],'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['password', 'string', 'min' => 6, 'max' => 255],
            ['username', 'unique',
                'targetClass' => User::className(),
                'message' => 'User isset'],
            ['email', 'email'],
            ['email', 'unique',
                'targetClass' => User::className(),
                'message' => 'Email isset']
        ];
    }
    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'email' => 'E-mail',
            'password' => 'password'
        ];
    }
    public function reg()
    {
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        //$user->generateSecretKey();
        $user->generateAccessToken();
        return $user->save() ? $user : null;
    }

}