<?php
/**
 * Created by PhpStorm.
 * User: �����
 * Date: 01.01.2016
 * Time: 20:28
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Books;

class BooksSearch extends Books
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id'], 'integer'],
            [['name', 'date_create'], 'safe'],
        ];
    }
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        $scenarios = Model::scenarios();
       // $scenarios['search'] = ['name', 'author_id', 'date_create'];
        return $scenarios;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Books::find();

        $this->load($params);

        if ($this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            if(!empty($params)) {
                foreach($params as $p=>$value) {
                    switch($p) {
                        case 'author':
                            if(!empty($value))
                                 $query->andFilterWhere(['author_id' => $value]);
                            break;
                        case 'keyword':
                            if(!empty($value))
                                $query->andFilterWhere(['LIKE', 'name', $value]);
                            break;
                        case 'start':
                            if(!empty($value)) {
                                $time = strtotime($value);
                                $query->andWhere('date_create >= :dc', [':dc'=>$time]);
                            }
                            break;
                        case 'end':
                            if(!empty($value)) {
                                $time = strtotime($value);
                                $query->andWhere('date_create <= :dc', [':dc'=>$time]);
                            }
                            break;
                    }
                }
            }
        }else{
            $query->where('0=1');
           // return $dataProvider;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

}