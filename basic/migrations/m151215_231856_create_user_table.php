<?php

use yii\db\Migration;

class m151215_231856_create_user_table extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username'=>'string NOT NULL',
            'email'=>'string NOT NULL',
            'password'=>'string NOT NULL',
            'created'=>'integer',
            'update'=>'integer',
            'auth_key'=>'string NOT NULL',
            'secret_key'=>'string NOT NULL',
            'access_token'=>'string NOT NULL',
        ]);
    }

    public function down()
    {
        $this->dropTable('user');
    }
}
