<?php

use yii\db\Migration;

class m160107_190323_insert_data_user extends Migration
{
    public function up()
    {
        $this->insert('user', [
            'username'=>'admin',
            'password'=>'$2y$13$LgRyk8dI6iSss3x0AvjEFu5q50A3YB3lzSRwR5QdkXLgHkbgE6Ahe',
            'email'=>'admin@admin.com',
            'created'=>'1451761141',
            'update'=>'1451761141',
            'access_token'=>'Miv7CrvatEGW3V6cveWJB1Ik9-HKBJbpa',
            'auth_key'=>'axV5lTlRH65Oe-WjdBYRPWNGgHpx378I'
        ]);
    }

    public function down()
    {
        echo "m160107_190323_insert_data_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
