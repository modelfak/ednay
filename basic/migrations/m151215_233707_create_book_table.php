<?php

use yii\db\Migration;

class m151215_233707_create_book_table extends Migration
{
    public function up()
    {
        $this->createTable('books', [
            'id' => $this->primaryKey(),
            'name' => 'text',
            'date_create'=>'integer',
            'date_publish'=>'integer',
            'preview'=>'text',
            'author_id'=>'integer'
        ]);

        $this->createIndex('idx-books-author_id', 'books', 'author_id');
        $this->addForeignKey('book_author_id', 'books', 'author_id', 'authors', 'id');
    }

    public function down()
    {
        $this->dropTable('books');
    }
}
