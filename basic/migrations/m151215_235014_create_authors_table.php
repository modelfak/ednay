<?php

use yii\db\Migration;

class m151215_235014_create_authors_table extends Migration
{
    public function up()
    {
        $this->createTable('authors', [
            'id' => $this->primaryKey(),
            'firstname'=>'text',
            'lastname'=>'text'
        ]);

    }

    public function down()
    {
        $this->dropTable('authors');
    }
}
