<?php

use yii\db\Migration;
use yii\db\Schema;


class m151217_231622_add_secret extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'secret_key', Schema::TYPE_STRING);
    }

    public function down()
    {
        echo "m151217_231622_add_secret cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
