<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app="mainApp">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body ng-controller="index">
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menu_it = [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'About', 'url' => ['/site/about']],
        ['label' => 'Contact', 'url' => ['/site/contact']],
        ['label' => 'Books', 'url' => ['/books']]
    ];
    if(Yii::$app->user->isGuest) :
        $menu_it[] = ['label' => 'Login', 'url' => ['/site/login']];
        $menu_it[] = ['label' => 'Register', 'url' => ['/site/signup']];
    else:
        $menu_it[] = ['label' => 'Logout', 'url' => ['/site/logout'], 'linkOptions'=>['data-method'=>'post']];
    endif;
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menu_it
//           if(Yii::$app->user->isGuest) {
//               ['label' => 'Login', 'url' => ['/site/login']],
//                ['label' => 'Register', 'url' => ['/site/login']]
//            } else {
//               '<li>'
//               . Html::beginForm(['/site/logout'], 'post')
//               . Html::submitButton(
//                   'Logout (' . Yii::$app->user->identity->username . ')',
//                   ['class' => 'btn btn-link']
//               )
//               . Html::endForm()
//               . '</li>'
//            }

    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
