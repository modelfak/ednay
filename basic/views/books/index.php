<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Books');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Books'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
      <?php echo Html::beginForm('/books/index', 'get', ['class'=>'form-inline']); ?>
            <?php $listData=ArrayHelper::map(app\models\Authors::find()->all(),'id','firstname')  ?>
             <div class="form-group">
                <?= Html::dropDownList('author', Yii::$app->getRequest()->getQueryParam('author'), $listData,  ['prompt'=>'Выберите автора', 'class'=>'form-control', 'style'=>'max-width:200px']) ?>
            </div>
            <div class="form-group">
                <?= Html::textInput('keyword', Yii::$app->getRequest()->getQueryParam('keyword'), ['placeholder'=>'Введите название книги', 'class'=>'form-control', 'style'=>'max-width:200px']) ?>
            </div>
            <br><br>
            <div class="form-group">
                <label>Дата выхода книги:</label>
            </div><br><br>
            <div class="form-group">
                <label for="start">От:</label>
                <?php
                echo \yii\jui\DatePicker::widget([
                    'name'  => 'start',
                    'value'  => Yii::$app->getRequest()->getQueryParam('start'),
                    'clientOptions'=>[
                        'changeYear'=>true
                    ],
                    'dateFormat' => 'yyyy-MM-dd'
                ]);
                 ?>
            </div>
            <div class="form-group">
                <label for="end">До:</label>
                <?php
                    echo \yii\jui\DatePicker::widget([
                        'name'  => 'end',
                        'value'  => Yii::$app->getRequest()->getQueryParam('end'),
                        'clientOptions'=>[
                            'changeYear'=>true
                        ],
                        'dateFormat' => 'yyyy-MM-dd'
                    ]);
                    //= Html::textInput('end', '', ['class'=>'form-control', 'id'=>'end', 'style'=>'max-width:200px'])
                ?>
            </div>
            <br><br>
            <?= Html::submitButton('Найти', ['class'=>'btn btn-default']) ?>
      <?php echo Html::endForm(); ?>
    <br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name:ntext',
            [
                'attribute' => 'preview',
                'format' => 'raw',
                'value'=>function($data) { return Html::a(Html::img($data->imageurlThumb,
                    ['width' => '70px']), $data->imageurl, ['rel' => 'fancybox']); }
            ],
            [
                'attribute' => 'date_create',
                'format' => ['date', 'php:d/m/Y']
            ],
            [
                'attribute' => 'date_publish',
                'format' => ['date', 'php:d/m/Y']
            ],
            [
                'attribute' => 'author',
                'value' => 'author.firstname'
            ],
            // 'preview:ntext',
            // 'date',
            // 'author_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

<?php
echo newerton\fancybox\FancyBox::widget([
    'target' => 'a[rel=fancybox]',
    'helpers' => true,
    'mouse' => true,
    'config' => [
        'maxWidth' => '90%',
        'maxHeight' => '90%',
        'playSpeed' => 7000,
        'padding' => 0,
        'fitToView' => false,
        'width' => '70%',
        'height' => '70%',
        'autoSize' => false,
        'closeClick' => false,
        'openEffect' => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect' => 'elastic',
        'nextEffect' => 'elastic',
        'closeBtn' => false,
        'openOpacity' => true,
        'helpers' => [
            'title' => ['type' => 'float'],
            'buttons' => [],
            'thumbs' => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)'
                ]
            ]
        ],
    ]
]);
