<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use dosamigos\fileinput\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Books */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->input(['rows' => 6]) ?>

    <?= $form->field($model, 'date_create')->widget(\yii\jui\DatePicker::classname(), [
        'language' => 'ru',
        'clientOptions'=>[
            'changeYear'=>true,
        ],
        'dateFormat' => 'yyyy-MM-dd'
    ]); ?>

    <?= $form->field($model, 'date_publish')->widget(\yii\jui\DatePicker::classname(), [
        'language' => 'ru',

        'clientOptions'=>[
            'changeYear'=>true
        ],
        'dateFormat' => 'yyyy-MM-dd'
    ]); ?>

    <?php
//          echo $form->field($model, 'preview')->widget(\dosamigos\fileinput\BootstrapFileInput::className(), [
//                'options' => ['accept' => 'uploads', 'multiple' => false],
//                'clientOptions' => [
//                    'previewFileType' => 'text',
//                    'browseClass' => 'btn btn-success',
//                    'uploadClass' => 'btn btn-info',
//                    'removeClass' => 'btn btn-danger',
//                    'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> '
//                ]
//
//            ]);
    echo FileInput::widget([
        'model' => $model,
        'attribute' => 'preview', // image is the attribute
        // using STYLE_IMAGE allows me to display an image. Cool to display previously
        // uploaded images
        'thumbnail' => Html::img($model->imageurl),
        'style' => FileInput::STYLE_IMAGE
    ]);
     ?>

    <?php $listData=ArrayHelper::map(app\models\Authors::find()->all(),'id','firstname')  ?>
    <?= $form->field($model, 'author_id')->dropDownList($listData,  ['prompt'=>'select author']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
