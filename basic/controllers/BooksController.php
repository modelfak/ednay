<?php

namespace app\controllers;

use Yii;
use app\models\Books;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\BooksSearch;
use yii\filters\AccessControl;

/**
 * BooksController implements the CRUD actions for Books model.
 */
class BooksController extends BehaviorsController
{

    public function actionIndex()
    {
        $searchModel = new BooksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Books model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Books model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Books();
        //$model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'preview');

           if($model->validate()) {
               if(!empty($file)) {
                   /* �������� ��� ����� � ��������� */
                   $img_name = Yii::$app->getSecurity()->generateRandomString();
                   $file->saveAs(Books::pathFile . $img_name . '.' . $file->extension);
                   /* resize image */
                   \yii\imagine\Image::thumbnail(Books::pathFile . $img_name . '.' . $file->extension, Books::widthThumb, Books::heightThumb)
                       ->save(Books::pathFileThumb . $img_name . '.' . $file->extension);
                   $model->preview = $img_name. '.' . $file->extension;
               }
               $model->save();
               return $this->redirect(['view', 'id' => $model->id]);
           }
        }
            return $this->render('create', [
                'model' => $model,
            ]);

    }

    /**
     * Updates an existing Books model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
       // $model->scenario = 'update';
        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'preview');
            //var_dump($model->getErrors());exit;

            if($model->validate()) {
                if(!empty($file)) {
                    /* �������� ��� ����� � ��������� */
                    if(!empty($model->preview) && file_exists(Books::pathFile . $model->preview)) {
                        @unlink(Books::pathFile . $model->preview);
                    }

                    $img_name = Yii::$app->getSecurity()->generateRandomString();
                    $file->saveAs(Books::pathFile . $img_name . '.' . $file->extension);
                    \yii\imagine\Image::thumbnail(Books::pathFile . $img_name . '.' . $file->extension, Books::widthThumb, Books::heightThumb)
                        ->save(Books::pathFileThumb . $img_name . '.' . $file->extension);
                    $model->preview = $img_name. '.' . $file->extension;
                }
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Books model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Books model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Books the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Books::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
