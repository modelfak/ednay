<?php
/**
 * Created by PhpStorm.
 * User: Роман
 */

namespace app\controllers;

use yii\rest\ActiveController;

class AuthorsController extends ActiveController
{
    // adjust the model class to match your model
    public $modelClass = 'app\models\Authors';

    public function behaviors()
    {
        return
            \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
                'corsFilter' => [
                    'class' => \yii\filters\Cors::className(),
                ],
            ]);
    }
}