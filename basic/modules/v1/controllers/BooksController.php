<?php
/**
 * Created by PhpStorm.
 * User: Роман
 */

namespace app\modules\v1\controllers;

use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\AccessControl;
use app\models;


class BooksController extends ActiveController
{
    // adjust the model class to match your model
    public $modelClass = 'app\models\Books';
    public $username = null;

    public function behaviors()
    {
        return
            \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
                'corsFilter' => [
                    'class' => \yii\filters\Cors::className(),
                ],
                'authenticator' => [
                    'class' => CompositeAuth::className(),
                   // 'auth' => [$this, 'auth'],
                    'authMethods' => [
                        HttpBasicAuth::className(),
                        HttpBearerAuth::className(),
                        QueryParamAuth::className(),
                    ],
                ],
                'access' => [
                    'class' => AccessControl::className(),
                    //'only' => ['index', 'create', 'update', 'view'],
                    'rules' => [
                        [
                            'actions' => ['index', 'create', 'update', 'view'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ]

            ]);
    }

    public function actions()
    {
        $actions = parent::actions();
        //unset($actions['update'], $actions['create']);
        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['create'] =  [
            'class' => 'yii\rest\RestFullBookCreate',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
            'scenario' => $this->createScenario,
        ];

        $actions['update'] = [

            'class' => 'yii\rest\RestFullBookUpdate',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
            'scenario' => $this->updateScenario,
        ];

        return $actions;
    }


    public function Auth($username, $password) {
        // username, password are mandatory fields
        //$us = new \app\models\User();
        if(empty($username) || empty($password))
            return null;

        // get user using requested email
        $user = \app\models\User::findOne([
            'username' => $username,
        ]);

        // if no record matching the requested user
        if(empty($user))
            return null;

        // hashed password from user record
        $this->username = $user->username;

        // validate password

        $isPass = $user->validatePassword($password);

        // if password validation fails
        if(!$isPass)
            return null;

        // if user validates (both user_email, user_password are valid)
        return $user;
    }
}